import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MainApp extends Application {
	private Stage primaryStage;
	private LoginWindow loginWindow;
	private TabPane tabPane;
	private OpretProduktPane OpretProduktPane;
	private VisSalgsPane VisSalgsPane;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		primaryStage.setTitle("Aarhus Bryghus SQL");
		primaryStage.setResizable(true);
		GridPane pane = new GridPane();
		pane.setGridLinesVisible(true);

		this.loginWindow = new LoginWindow("Login", this.primaryStage);
		loginWindow.showAndWait();

		this.initContent(pane);

		Scene scene = new Scene(pane);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	private void initContent(GridPane pane) {

		GridPane paneLoginInformation = new GridPane();
		paneLoginInformation.setPadding(new Insets(10));
		paneLoginInformation.setHgap(10);
		paneLoginInformation.setVgap(10);
		paneLoginInformation.setGridLinesVisible(false);

		this.tabPane = new TabPane();
		this.initTabPane(tabPane);

		pane.add(tabPane, 0, 0);
		this.updateLoginInformation();

	}

	private void initTabPane(TabPane tabPane) {
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		Tab tabOpretProdukt = new Tab("Opret Produkt");
		tabPane.getTabs().add(tabOpretProdukt);

		OpretProduktPane = new OpretProduktPane();
		tabOpretProdukt.setContent(OpretProduktPane);

		Tab tabVisSalg = new Tab("Vis Salg");
		tabPane.getTabs().add(tabVisSalg);

		VisSalgsPane = new VisSalgsPane();
		tabVisSalg.setContent(VisSalgsPane);
		tabVisSalg.setOnSelectionChanged(event -> VisSalgsPane.updateControls());

	}

	public void updateLoginInformation() {
		this.tabPane.getSelectionModel().select(0);
	}

}
