
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLcon {
	private static SQLcon sqlcon;
	private PreparedStatement pstmt = null;
	private Statement stmt = null;
	private Connection conn;
	private String DBServer;
	private String DBDatabase;
	private String DBUsername;
	private String DBPassword;

	private SQLcon() {
	}

	public void setDBServer(String DBServer) {
		this.DBServer = DBServer;
	}

	public void setDBDatabase(String DBDatabase) {
		this.DBDatabase = DBDatabase;
	}

	public void setDBUsername(String DBUsername) {
		this.DBUsername = DBUsername;
	}

	public void setDBPassword(String DBPassword) {
		this.DBPassword = DBPassword;
	}

	public String getDBServer() {
		return DBServer;
	}

	public String getDBDatabase() {
		return DBDatabase;
	}

	public String getDBUsername() {
		return DBUsername;
	}

	public String getDBPassword() {
		return DBPassword;
	}

	public static SQLcon getInstance() {
		if (sqlcon == null) {
			sqlcon = new SQLcon();
		}
		return sqlcon;
	}

	public Connection connect() {

		String dbs = "jdbc:sqlserver://" + DBServer + ";database=" + DBDatabase;

		// String dbURL = "jdbc:sqlserver://mssql4.gear.host";
		// String user = "skole1";
		// String pass = "Kw4XhVM~3C0_";
		// String datab = "skole1";

		try {
			conn = DriverManager.getConnection(dbs + ";username=" + DBUsername + ";password=" + DBPassword + ";");
		} catch (SQLException e) {

		}
		return conn;
	}

	public void open(int type) {
		try {
			conn.setTransactionIsolation(type);
			conn.setAutoCommit(false);
			stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
		} catch (SQLException e) {

		}
	}

	public void close() {
		try {
			conn.commit();
			conn.setAutoCommit(true);
			stmt.close();
			conn.close();
		} catch (SQLException e) {

		}
	}

	public ResultSet query(String SQL) {
		try {
			return stmt.executeQuery(SQL);
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
		return null;
	}

	public PreparedStatement pstmt(String SQL) throws Exception {
		try {
			pstmt = conn.prepareStatement(SQL);
			// rs = pstmt.executeQuery(sql);
		} catch (SQLException ex) {
			System.err.println("Error:" + ex.getMessage());
		}
		return pstmt;
	}

	public void update(String SQL) {
		try {
			stmt.executeUpdate(SQL);
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	public void delete(String SQL) {
		try {
			stmt.executeQuery(SQL);
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

}
