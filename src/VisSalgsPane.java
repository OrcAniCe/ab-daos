import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import javafx.geometry.Insets;
import javafx.scene.control.ListView;

public class VisSalgsPane extends Pane {
	private ListView<String> lvwEks;
	ArrayList<String> salgsliste;

	public VisSalgsPane() {

		setPadding(new Insets(20));
		setHgap(20);
		setVgap(10);
		setGridLinesVisible(false);

		lvwEks = new ListView<>();
		this.add(lvwEks, 0, 1, 2, 2);
		lvwEks.setPrefWidth(200);
		lvwEks.setPrefHeight(100);

	}

	public void updateControls() {
		salgsliste = new ArrayList<String>();
		try {
			SQLcon sql = SQLcon.getInstance();
			sql.connect();
			sql.open(Connection.TRANSACTION_SERIALIZABLE);

			ResultSet res = sql.query("exec pSalgsoversigtDato " + "'2017-09-08'");
			while (res.next()) {
				salgsliste.add(res.getString(1) + " " + res.getString(2) + ",-");
				lvwEks.getItems().setAll(salgsliste);
			}
		} catch (Exception e) {
			System.out.println("fejl:  " + e.getMessage());
		}
	}

}
