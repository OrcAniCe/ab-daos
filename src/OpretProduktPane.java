import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

public class OpretProduktPane extends Pane {
	private Button btnOpret;
	private Label lblPT;
	private Label lblNavn;
	private Label lblLA;
	private TextField txfProduktType;
	private TextField txfNavn;
	private TextField txfLagerAntal;
	private Label lblError;

	public OpretProduktPane() {

		setPadding(new Insets(20));
		setHgap(20);
		setVgap(10);
		setGridLinesVisible(false);

		lblPT = new Label("ProduktType:");
		this.add(lblPT, 0, 1);

		txfProduktType = new TextField();
		txfProduktType.setPrefWidth(150);
		txfProduktType.setEditable(true);
		this.add(txfProduktType, 1, 1);

		lblNavn = new Label("Navn:");
		this.add(lblNavn, 0, 2);

		txfNavn = new TextField();
		txfNavn.setPrefWidth(150);
		txfNavn.setEditable(true);
		this.add(txfNavn, 1, 2);

		lblLA = new Label("LagerAntal:");
		this.add(lblLA, 0, 3);

		txfLagerAntal = new TextField();
		txfLagerAntal.setPrefWidth(150);
		txfLagerAntal.setEditable(true);
		this.add(txfLagerAntal, 1, 3);

		btnOpret = new Button("Opret");
		this.add(btnOpret, 0, 4);
		btnOpret.setOnAction(event -> OpretProdukt());
		lblError = new Label("");
		lblError.setTextFill(Color.RED);
		this.add(lblError, 0, 6);
	}

	private void OpretProdukt() {
		String header = "";
		String message = "";
		SQLcon sql = SQLcon.getInstance();
		sql.connect();
		sql.open(Connection.TRANSACTION_SERIALIZABLE);

		PreparedStatement pstmt;
		try {
			pstmt = sql.pstmt("exec pOpretProdukt ?,?,?");
			pstmt.setString(1, txfProduktType.getText());
			pstmt.setString(2, txfNavn.getText());
			pstmt.setString(3, txfLagerAntal.getText());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			header = "Fejl i SQL";
			switch (e.getErrorCode()) {
			case 547: {
				if (e.getMessage().indexOf("") != -1) {
					message = "Angiv en eksisterende ProduktType";
				}
				break;
			}
			case 8114: {
				if (e.getMessage().indexOf("") != -1) {
					message = "LagerAntal skal være en int";
				}
				break;
			}
			case 2627: {
				if (e.getMessage().indexOf("") != -1) {
					message = "Produktet eksisterer allerede under denne ProduktType";
				}
				break;
			}
			default:
				message = "fejlSQL: " + e.getMessage() + " - " + e.getErrorCode();
			}
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error");
			alert.setHeaderText(header);
			alert.setContentText(message);

			alert.showAndWait();
		} catch (Exception e) {
			System.out.println("fejl: " + e.getMessage());
		}

		sql.close();
	}

}
