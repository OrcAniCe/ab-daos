import java.net.InetAddress;
import java.net.UnknownHostException;

import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class LoginWindow extends Stage {
	private Stage primaryStage;
	public static int loginTimes = 0;
	static String DBServer;
	static String DBDatabase;
	static String DBUsername;
	static String DBPassword;

	public LoginWindow(String title, Stage primaryStage) {
		this.primaryStage = primaryStage;

		this.setResizable(false);
		this.setOnCloseRequest(e -> this.onCloseLoginWindow());

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	// -------------------------------------------------------------------------

	private TextField txfServer;
	private TextField txfDatabase;
	private TextField txfUsername;
	private PasswordField txfPassword;
	private Label lblError;

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		txfServer = new TextField();
		pane.add(txfServer, 0, 1);
		txfServer.setPrefWidth(200);

		txfDatabase = new TextField();
		pane.add(txfDatabase, 0, 2);
		txfDatabase.setPrefWidth(200);

		txfUsername = new TextField();
		pane.add(txfUsername, 0, 3);
		txfUsername.setPrefWidth(200);

		txfPassword = new PasswordField();
		pane.add(txfPassword, 0, 4);

		Button btnForbind = new Button("Forbind");
		pane.add(btnForbind, 0, 5);
		GridPane.setHalignment(btnForbind, HPos.LEFT);
		btnForbind.setOnAction(event -> this.forbindAction());

		lblError = new Label();
		pane.add(lblError, 0, 5);
		lblError.setStyle("-fx-text-fill: red");

		this.initControls();
	}

	/**
	 * Finds the computers hostname and adds it to the connection string
	 * http://stackoverflow.com/questions/7883542/getting-the-computer-name-in-java
	 *
	 * @return
	 */
	private static String getComputerName() {
		String hostname = "Unknown";
		try {
			InetAddress addr;
			addr = InetAddress.getLocalHost();
			hostname = addr.getHostName();
		} catch (UnknownHostException ex) {
			System.out.println("Hostname can not be resolved");
		}
		return hostname;
	}

	private void initControls() {
		String ComputerHostName = getComputerName();
		txfServer.setText(ComputerHostName);
		txfDatabase.setText("Database");
		txfUsername.setText("Username");
		txfPassword.setText("Password");
	}

	private void forbindAction() {
		SQLcon sql = SQLcon.getInstance();
		sql.setDBServer(txfServer.getText().trim());
		sql.setDBDatabase(txfDatabase.getText().trim());
		sql.setDBUsername(txfUsername.getText().trim());
		sql.setDBPassword(txfPassword.getText().trim());
		if (sql.connect() == null) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error");
			alert.setHeaderText("Could not connect to database");
			alert.setContentText("The program was unable to connect to the database. Please try again");
			alert.show();
			return;
		}
		this.initControls();
		this.hide();
		this.primaryStage.show();

	}

	private void onCloseLoginWindow() {
		Platform.exit();
	}

}
